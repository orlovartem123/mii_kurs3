﻿namespace Lab1
{
    partial class FormMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.buttonX = new System.Windows.Forms.Button();
            this.textBoxX = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxSettings = new System.Windows.Forms.GroupBox();
            this.checkBoxNeedDrawUnitLine = new System.Windows.Forms.CheckBox();
            this.labelNumbersFontSize = new System.Windows.Forms.Label();
            this.numericUpDownNumbersFontSize = new System.Windows.Forms.NumericUpDown();
            this.labelPadding = new System.Windows.Forms.Label();
            this.numericUpDownPadding = new System.Windows.Forms.NumericUpDown();
            this.labelDotTextFillY = new System.Windows.Forms.Label();
            this.numericUpDownDotTextFillY = new System.Windows.Forms.NumericUpDown();
            this.labelDotTextFillX = new System.Windows.Forms.Label();
            this.numericUpDownDotTextFillX = new System.Windows.Forms.NumericUpDown();
            this.labelMaxY = new System.Windows.Forms.Label();
            this.numericUpDownMaxY = new System.Windows.Forms.NumericUpDown();
            this.textBoxE = new System.Windows.Forms.TextBox();
            this.labelE = new System.Windows.Forms.Label();
            this.labelIterations = new System.Windows.Forms.Label();
            this.numericUpDownIterations = new System.Windows.Forms.NumericUpDown();
            this.labelScaleY = new System.Windows.Forms.Label();
            this.numericUpDownScaleY = new System.Windows.Forms.NumericUpDown();
            this.buttonSaveSettings = new System.Windows.Forms.Button();
            this.labelPixel = new System.Windows.Forms.Label();
            this.numericUpDownPixel = new System.Windows.Forms.NumericUpDown();
            this.labelScaleX = new System.Windows.Forms.Label();
            this.numericUpDownScaleX = new System.Windows.Forms.NumericUpDown();
            this.labelMaxX = new System.Windows.Forms.Label();
            this.numericUpDownMaxX = new System.Windows.Forms.NumericUpDown();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.данныеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.трапецииToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.кластерыИТочкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.временнойРядToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBoxImplication = new System.Windows.Forms.GroupBox();
            this.groupBoxCluster = new System.Windows.Forms.GroupBox();
            this.buttonCluster = new System.Windows.Forms.Button();
            this.groupBoxMonitor = new System.Windows.Forms.GroupBox();
            this.labelMonitorSettings = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.labelMonitorTimeSet = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelMonitorDots = new System.Windows.Forms.Label();
            this.labelMonitorCluster = new System.Windows.Forms.Label();
            this.labelMonitorTrap = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonTend = new System.Windows.Forms.Button();
            this.textBoxM = new System.Windows.Forms.TextBox();
            this.labelM = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.groupBoxSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNumbersFontSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPadding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDotTextFillY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDotTextFillX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIterations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownScaleY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPixel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownScaleX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxX)).BeginInit();
            this.menuStrip.SuspendLayout();
            this.groupBoxImplication.SuspendLayout();
            this.groupBoxCluster.SuspendLayout();
            this.groupBoxMonitor.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox
            // 
            this.pictureBox.BackColor = System.Drawing.Color.White;
            this.pictureBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox.Location = new System.Drawing.Point(0, 24);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(950, 284);
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            // 
            // buttonX
            // 
            this.buttonX.Location = new System.Drawing.Point(138, 21);
            this.buttonX.Name = "buttonX";
            this.buttonX.Size = new System.Drawing.Size(75, 23);
            this.buttonX.TabIndex = 1;
            this.buttonX.Text = "Расчет";
            this.buttonX.UseVisualStyleBackColor = true;
            // 
            // textBoxX
            // 
            this.textBoxX.Location = new System.Drawing.Point(26, 23);
            this.textBoxX.Name = "textBoxX";
            this.textBoxX.Size = new System.Drawing.Size(100, 20);
            this.textBoxX.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(14, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "X";
            // 
            // groupBoxSettings
            // 
            this.groupBoxSettings.Controls.Add(this.textBoxM);
            this.groupBoxSettings.Controls.Add(this.labelM);
            this.groupBoxSettings.Controls.Add(this.checkBoxNeedDrawUnitLine);
            this.groupBoxSettings.Controls.Add(this.labelNumbersFontSize);
            this.groupBoxSettings.Controls.Add(this.numericUpDownNumbersFontSize);
            this.groupBoxSettings.Controls.Add(this.labelPadding);
            this.groupBoxSettings.Controls.Add(this.numericUpDownPadding);
            this.groupBoxSettings.Controls.Add(this.labelDotTextFillY);
            this.groupBoxSettings.Controls.Add(this.numericUpDownDotTextFillY);
            this.groupBoxSettings.Controls.Add(this.labelDotTextFillX);
            this.groupBoxSettings.Controls.Add(this.numericUpDownDotTextFillX);
            this.groupBoxSettings.Controls.Add(this.labelMaxY);
            this.groupBoxSettings.Controls.Add(this.numericUpDownMaxY);
            this.groupBoxSettings.Controls.Add(this.textBoxE);
            this.groupBoxSettings.Controls.Add(this.labelE);
            this.groupBoxSettings.Controls.Add(this.labelIterations);
            this.groupBoxSettings.Controls.Add(this.numericUpDownIterations);
            this.groupBoxSettings.Controls.Add(this.labelScaleY);
            this.groupBoxSettings.Controls.Add(this.numericUpDownScaleY);
            this.groupBoxSettings.Controls.Add(this.buttonSaveSettings);
            this.groupBoxSettings.Controls.Add(this.labelPixel);
            this.groupBoxSettings.Controls.Add(this.numericUpDownPixel);
            this.groupBoxSettings.Controls.Add(this.labelScaleX);
            this.groupBoxSettings.Controls.Add(this.numericUpDownScaleX);
            this.groupBoxSettings.Controls.Add(this.labelMaxX);
            this.groupBoxSettings.Controls.Add(this.numericUpDownMaxX);
            this.groupBoxSettings.Location = new System.Drawing.Point(490, 314);
            this.groupBoxSettings.Name = "groupBoxSettings";
            this.groupBoxSettings.Size = new System.Drawing.Size(448, 189);
            this.groupBoxSettings.TabIndex = 4;
            this.groupBoxSettings.TabStop = false;
            this.groupBoxSettings.Text = "Настройки";
            // 
            // checkBoxNeedDrawUnitLine
            // 
            this.checkBoxNeedDrawUnitLine.AutoSize = true;
            this.checkBoxNeedDrawUnitLine.Location = new System.Drawing.Point(322, 73);
            this.checkBoxNeedDrawUnitLine.Name = "checkBoxNeedDrawUnitLine";
            this.checkBoxNeedDrawUnitLine.Size = new System.Drawing.Size(119, 17);
            this.checkBoxNeedDrawUnitLine.TabIndex = 9;
            this.checkBoxNeedDrawUnitLine.Text = "Рисовать деления";
            this.checkBoxNeedDrawUnitLine.UseVisualStyleBackColor = true;
            this.checkBoxNeedDrawUnitLine.CheckedChanged += new System.EventHandler(this.checkBoxNeedDrawUnit_CheckedChanged);
            // 
            // labelNumbersFontSize
            // 
            this.labelNumbersFontSize.AutoSize = true;
            this.labelNumbersFontSize.ForeColor = System.Drawing.Color.Black;
            this.labelNumbersFontSize.Location = new System.Drawing.Point(319, 23);
            this.labelNumbersFontSize.Name = "labelNumbersFontSize";
            this.labelNumbersFontSize.Size = new System.Drawing.Size(38, 13);
            this.labelNumbersFontSize.TabIndex = 26;
            this.labelNumbersFontSize.Text = "Font S";
            // 
            // numericUpDownNumbersFontSize
            // 
            this.numericUpDownNumbersFontSize.Location = new System.Drawing.Point(373, 19);
            this.numericUpDownNumbersFontSize.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numericUpDownNumbersFontSize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownNumbersFontSize.Name = "numericUpDownNumbersFontSize";
            this.numericUpDownNumbersFontSize.Size = new System.Drawing.Size(69, 20);
            this.numericUpDownNumbersFontSize.TabIndex = 25;
            this.numericUpDownNumbersFontSize.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownNumbersFontSize.ValueChanged += new System.EventHandler(this.numericUpDownNumbersFontSize_ValueChanged);
            // 
            // labelPadding
            // 
            this.labelPadding.AutoSize = true;
            this.labelPadding.ForeColor = System.Drawing.Color.Black;
            this.labelPadding.Location = new System.Drawing.Point(186, 127);
            this.labelPadding.Name = "labelPadding";
            this.labelPadding.Size = new System.Drawing.Size(46, 13);
            this.labelPadding.TabIndex = 24;
            this.labelPadding.Text = "Padding";
            // 
            // numericUpDownPadding
            // 
            this.numericUpDownPadding.Location = new System.Drawing.Point(240, 123);
            this.numericUpDownPadding.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownPadding.Name = "numericUpDownPadding";
            this.numericUpDownPadding.Size = new System.Drawing.Size(69, 20);
            this.numericUpDownPadding.TabIndex = 23;
            this.numericUpDownPadding.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numericUpDownPadding.ValueChanged += new System.EventHandler(this.numericUpDownPadding_ValueChanged);
            // 
            // labelDotTextFillY
            // 
            this.labelDotTextFillY.AutoSize = true;
            this.labelDotTextFillY.ForeColor = System.Drawing.Color.Black;
            this.labelDotTextFillY.Location = new System.Drawing.Point(186, 101);
            this.labelDotTextFillY.Name = "labelDotTextFillY";
            this.labelDotTextFillY.Size = new System.Drawing.Size(39, 13);
            this.labelDotTextFillY.TabIndex = 22;
            this.labelDotTextFillY.Text = "Df % Y";
            // 
            // numericUpDownDotTextFillY
            // 
            this.numericUpDownDotTextFillY.Location = new System.Drawing.Point(240, 97);
            this.numericUpDownDotTextFillY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownDotTextFillY.Name = "numericUpDownDotTextFillY";
            this.numericUpDownDotTextFillY.Size = new System.Drawing.Size(69, 20);
            this.numericUpDownDotTextFillY.TabIndex = 21;
            this.numericUpDownDotTextFillY.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.numericUpDownDotTextFillY.ValueChanged += new System.EventHandler(this.numericUpDownDotTextFillY_ValueChanged);
            // 
            // labelDotTextFillX
            // 
            this.labelDotTextFillX.AutoSize = true;
            this.labelDotTextFillX.ForeColor = System.Drawing.Color.Black;
            this.labelDotTextFillX.Location = new System.Drawing.Point(186, 75);
            this.labelDotTextFillX.Name = "labelDotTextFillX";
            this.labelDotTextFillX.Size = new System.Drawing.Size(39, 13);
            this.labelDotTextFillX.TabIndex = 20;
            this.labelDotTextFillX.Text = "Df % X";
            // 
            // numericUpDownDotTextFillX
            // 
            this.numericUpDownDotTextFillX.Location = new System.Drawing.Point(240, 71);
            this.numericUpDownDotTextFillX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownDotTextFillX.Name = "numericUpDownDotTextFillX";
            this.numericUpDownDotTextFillX.Size = new System.Drawing.Size(69, 20);
            this.numericUpDownDotTextFillX.TabIndex = 19;
            this.numericUpDownDotTextFillX.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.numericUpDownDotTextFillX.ValueChanged += new System.EventHandler(this.numericUpDownDotTextFill_ValueChanged);
            // 
            // labelMaxY
            // 
            this.labelMaxY.AutoSize = true;
            this.labelMaxY.ForeColor = System.Drawing.Color.Black;
            this.labelMaxY.Location = new System.Drawing.Point(186, 23);
            this.labelMaxY.Name = "labelMaxY";
            this.labelMaxY.Size = new System.Drawing.Size(37, 13);
            this.labelMaxY.TabIndex = 18;
            this.labelMaxY.Text = "Max Y";
            // 
            // numericUpDownMaxY
            // 
            this.numericUpDownMaxY.Location = new System.Drawing.Point(240, 19);
            this.numericUpDownMaxY.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDownMaxY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownMaxY.Name = "numericUpDownMaxY";
            this.numericUpDownMaxY.Size = new System.Drawing.Size(69, 20);
            this.numericUpDownMaxY.TabIndex = 17;
            this.numericUpDownMaxY.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numericUpDownMaxY.ValueChanged += new System.EventHandler(this.numericUpDownMaxY_ValueChanged);
            // 
            // textBoxE
            // 
            this.textBoxE.Location = new System.Drawing.Point(240, 45);
            this.textBoxE.Name = "textBoxE";
            this.textBoxE.Size = new System.Drawing.Size(69, 20);
            this.textBoxE.TabIndex = 16;
            this.textBoxE.Text = "0.001";
            this.textBoxE.TextChanged += new System.EventHandler(this.textBoxE_TextChanged);
            // 
            // labelE
            // 
            this.labelE.AutoSize = true;
            this.labelE.Location = new System.Drawing.Point(186, 49);
            this.labelE.Name = "labelE";
            this.labelE.Size = new System.Drawing.Size(14, 13);
            this.labelE.TabIndex = 15;
            this.labelE.Text = "E";
            // 
            // labelIterations
            // 
            this.labelIterations.AutoSize = true;
            this.labelIterations.Location = new System.Drawing.Point(6, 125);
            this.labelIterations.Name = "labelIterations";
            this.labelIterations.Size = new System.Drawing.Size(91, 13);
            this.labelIterations.TabIndex = 14;
            this.labelIterations.Text = "Кол-во итераций";
            // 
            // numericUpDownIterations
            // 
            this.numericUpDownIterations.Location = new System.Drawing.Point(105, 123);
            this.numericUpDownIterations.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDownIterations.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownIterations.Name = "numericUpDownIterations";
            this.numericUpDownIterations.Size = new System.Drawing.Size(69, 20);
            this.numericUpDownIterations.TabIndex = 13;
            this.numericUpDownIterations.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericUpDownIterations.ValueChanged += new System.EventHandler(this.numericUpDownIterations_ValueChanged);
            // 
            // labelScaleY
            // 
            this.labelScaleY.AutoSize = true;
            this.labelScaleY.Location = new System.Drawing.Point(6, 73);
            this.labelScaleY.Name = "labelScaleY";
            this.labelScaleY.Size = new System.Drawing.Size(63, 13);
            this.labelScaleY.TabIndex = 12;
            this.labelScaleY.Text = "Масштаб Y";
            // 
            // numericUpDownScaleY
            // 
            this.numericUpDownScaleY.Location = new System.Drawing.Point(105, 71);
            this.numericUpDownScaleY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownScaleY.Name = "numericUpDownScaleY";
            this.numericUpDownScaleY.Size = new System.Drawing.Size(69, 20);
            this.numericUpDownScaleY.TabIndex = 11;
            this.numericUpDownScaleY.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownScaleY.ValueChanged += new System.EventHandler(this.numericUpDownScaleY_ValueChanged);
            // 
            // buttonSaveSettings
            // 
            this.buttonSaveSettings.Location = new System.Drawing.Point(367, 161);
            this.buttonSaveSettings.Name = "buttonSaveSettings";
            this.buttonSaveSettings.Size = new System.Drawing.Size(75, 22);
            this.buttonSaveSettings.TabIndex = 4;
            this.buttonSaveSettings.Text = "Сохранить";
            this.buttonSaveSettings.UseVisualStyleBackColor = true;
            this.buttonSaveSettings.Click += new System.EventHandler(this.buttonSaveSettings_Click);
            // 
            // labelPixel
            // 
            this.labelPixel.AutoSize = true;
            this.labelPixel.Location = new System.Drawing.Point(6, 99);
            this.labelPixel.Name = "labelPixel";
            this.labelPixel.Size = new System.Drawing.Size(51, 13);
            this.labelPixel.TabIndex = 10;
            this.labelPixel.Text = "Пиксель";
            // 
            // numericUpDownPixel
            // 
            this.numericUpDownPixel.Enabled = false;
            this.numericUpDownPixel.Location = new System.Drawing.Point(105, 97);
            this.numericUpDownPixel.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownPixel.Name = "numericUpDownPixel";
            this.numericUpDownPixel.Size = new System.Drawing.Size(69, 20);
            this.numericUpDownPixel.TabIndex = 9;
            this.numericUpDownPixel.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownPixel.ValueChanged += new System.EventHandler(this.numericUpDownPixel_ValueChanged);
            // 
            // labelScaleX
            // 
            this.labelScaleX.AutoSize = true;
            this.labelScaleX.Location = new System.Drawing.Point(6, 47);
            this.labelScaleX.Name = "labelScaleX";
            this.labelScaleX.Size = new System.Drawing.Size(63, 13);
            this.labelScaleX.TabIndex = 8;
            this.labelScaleX.Text = "Масштаб X";
            // 
            // numericUpDownScaleX
            // 
            this.numericUpDownScaleX.Location = new System.Drawing.Point(105, 45);
            this.numericUpDownScaleX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownScaleX.Name = "numericUpDownScaleX";
            this.numericUpDownScaleX.Size = new System.Drawing.Size(69, 20);
            this.numericUpDownScaleX.TabIndex = 7;
            this.numericUpDownScaleX.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownScaleX.ValueChanged += new System.EventHandler(this.numericUpDownScaleX_ValueChanged);
            // 
            // labelMaxX
            // 
            this.labelMaxX.AutoSize = true;
            this.labelMaxX.ForeColor = System.Drawing.Color.Black;
            this.labelMaxX.Location = new System.Drawing.Point(6, 21);
            this.labelMaxX.Name = "labelMaxX";
            this.labelMaxX.Size = new System.Drawing.Size(37, 13);
            this.labelMaxX.TabIndex = 6;
            this.labelMaxX.Text = "Max X";
            // 
            // numericUpDownMaxX
            // 
            this.numericUpDownMaxX.Location = new System.Drawing.Point(105, 19);
            this.numericUpDownMaxX.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDownMaxX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownMaxX.Name = "numericUpDownMaxX";
            this.numericUpDownMaxX.Size = new System.Drawing.Size(69, 20);
            this.numericUpDownMaxX.TabIndex = 0;
            this.numericUpDownMaxX.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericUpDownMaxX.ValueChanged += new System.EventHandler(this.numericUpDownMaxX_ValueChanged);
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.данныеToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(950, 24);
            this.menuStrip.TabIndex = 5;
            this.menuStrip.Text = "menuStrip1";
            // 
            // данныеToolStripMenuItem
            // 
            this.данныеToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.трапецииToolStripMenuItem,
            this.кластерыИТочкиToolStripMenuItem,
            this.временнойРядToolStripMenuItem});
            this.данныеToolStripMenuItem.Name = "данныеToolStripMenuItem";
            this.данныеToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.данныеToolStripMenuItem.Text = "Данные";
            // 
            // трапецииToolStripMenuItem
            // 
            this.трапецииToolStripMenuItem.Name = "трапецииToolStripMenuItem";
            this.трапецииToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.трапецииToolStripMenuItem.Text = "Трапеции";
            // 
            // кластерыИТочкиToolStripMenuItem
            // 
            this.кластерыИТочкиToolStripMenuItem.Name = "кластерыИТочкиToolStripMenuItem";
            this.кластерыИТочкиToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.кластерыИТочкиToolStripMenuItem.Text = "Кластеры и точки";
            this.кластерыИТочкиToolStripMenuItem.Click += new System.EventHandler(this.кластерыИТочкиToolStripMenuItem_Click);
            // 
            // временнойРядToolStripMenuItem
            // 
            this.временнойРядToolStripMenuItem.Name = "временнойРядToolStripMenuItem";
            this.временнойРядToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.временнойРядToolStripMenuItem.Text = "Временной ряд";
            // 
            // groupBoxImplication
            // 
            this.groupBoxImplication.Controls.Add(this.label1);
            this.groupBoxImplication.Controls.Add(this.textBoxX);
            this.groupBoxImplication.Controls.Add(this.buttonX);
            this.groupBoxImplication.Location = new System.Drawing.Point(12, 314);
            this.groupBoxImplication.Name = "groupBoxImplication";
            this.groupBoxImplication.Size = new System.Drawing.Size(223, 59);
            this.groupBoxImplication.TabIndex = 6;
            this.groupBoxImplication.TabStop = false;
            this.groupBoxImplication.Text = "Импликация и принадлежность";
            // 
            // groupBoxCluster
            // 
            this.groupBoxCluster.Controls.Add(this.buttonCluster);
            this.groupBoxCluster.Location = new System.Drawing.Point(12, 379);
            this.groupBoxCluster.Name = "groupBoxCluster";
            this.groupBoxCluster.Size = new System.Drawing.Size(223, 59);
            this.groupBoxCluster.TabIndex = 7;
            this.groupBoxCluster.TabStop = false;
            this.groupBoxCluster.Text = "Кластетирация";
            // 
            // buttonCluster
            // 
            this.buttonCluster.Location = new System.Drawing.Point(70, 24);
            this.buttonCluster.Name = "buttonCluster";
            this.buttonCluster.Size = new System.Drawing.Size(75, 23);
            this.buttonCluster.TabIndex = 4;
            this.buttonCluster.Text = "Расчет";
            this.buttonCluster.UseVisualStyleBackColor = true;
            this.buttonCluster.Click += new System.EventHandler(this.buttonCluster_Click);
            // 
            // groupBoxMonitor
            // 
            this.groupBoxMonitor.Controls.Add(this.labelMonitorSettings);
            this.groupBoxMonitor.Controls.Add(this.label13);
            this.groupBoxMonitor.Controls.Add(this.labelMonitorTimeSet);
            this.groupBoxMonitor.Controls.Add(this.label8);
            this.groupBoxMonitor.Controls.Add(this.labelMonitorDots);
            this.groupBoxMonitor.Controls.Add(this.labelMonitorCluster);
            this.groupBoxMonitor.Controls.Add(this.labelMonitorTrap);
            this.groupBoxMonitor.Controls.Add(this.label7);
            this.groupBoxMonitor.Controls.Add(this.label6);
            this.groupBoxMonitor.Controls.Add(this.label5);
            this.groupBoxMonitor.Location = new System.Drawing.Point(241, 314);
            this.groupBoxMonitor.Name = "groupBoxMonitor";
            this.groupBoxMonitor.Size = new System.Drawing.Size(243, 189);
            this.groupBoxMonitor.TabIndex = 8;
            this.groupBoxMonitor.TabStop = false;
            this.groupBoxMonitor.Text = "Мониторинг";
            // 
            // labelMonitorSettings
            // 
            this.labelMonitorSettings.AutoSize = true;
            this.labelMonitorSettings.Location = new System.Drawing.Point(68, 75);
            this.labelMonitorSettings.Name = "labelMonitorSettings";
            this.labelMonitorSettings.Size = new System.Drawing.Size(42, 13);
            this.labelMonitorSettings.TabIndex = 9;
            this.labelMonitorSettings.Text = "NoText";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 75);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(62, 13);
            this.label13.TabIndex = 8;
            this.label13.Text = "Настройки";
            // 
            // labelMonitorTimeSet
            // 
            this.labelMonitorTimeSet.AutoSize = true;
            this.labelMonitorTimeSet.Location = new System.Drawing.Point(68, 62);
            this.labelMonitorTimeSet.Name = "labelMonitorTimeSet";
            this.labelMonitorTimeSet.Size = new System.Drawing.Size(42, 13);
            this.labelMonitorTimeSet.TabIndex = 7;
            this.labelMonitorTimeSet.Text = "NoText";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 62);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Врем.ряд";
            // 
            // labelMonitorDots
            // 
            this.labelMonitorDots.AutoSize = true;
            this.labelMonitorDots.Location = new System.Drawing.Point(68, 49);
            this.labelMonitorDots.Name = "labelMonitorDots";
            this.labelMonitorDots.Size = new System.Drawing.Size(42, 13);
            this.labelMonitorDots.TabIndex = 5;
            this.labelMonitorDots.Text = "NoText";
            // 
            // labelMonitorCluster
            // 
            this.labelMonitorCluster.AutoSize = true;
            this.labelMonitorCluster.Location = new System.Drawing.Point(68, 36);
            this.labelMonitorCluster.Name = "labelMonitorCluster";
            this.labelMonitorCluster.Size = new System.Drawing.Size(42, 13);
            this.labelMonitorCluster.TabIndex = 4;
            this.labelMonitorCluster.Text = "NoText";
            // 
            // labelMonitorTrap
            // 
            this.labelMonitorTrap.AutoSize = true;
            this.labelMonitorTrap.Location = new System.Drawing.Point(68, 23);
            this.labelMonitorTrap.Name = "labelMonitorTrap";
            this.labelMonitorTrap.Size = new System.Drawing.Size(42, 13);
            this.labelMonitorTrap.TabIndex = 3;
            this.labelMonitorTrap.Text = "NoText";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Точки";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 36);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Кластеры";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Трапеции";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonTend);
            this.groupBox1.Location = new System.Drawing.Point(12, 444);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(223, 59);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Тенденции";
            // 
            // buttonTend
            // 
            this.buttonTend.Location = new System.Drawing.Point(70, 24);
            this.buttonTend.Name = "buttonTend";
            this.buttonTend.Size = new System.Drawing.Size(75, 23);
            this.buttonTend.TabIndex = 4;
            this.buttonTend.Text = "Расчет";
            this.buttonTend.UseVisualStyleBackColor = true;
            // 
            // textBoxM
            // 
            this.textBoxM.Location = new System.Drawing.Point(373, 45);
            this.textBoxM.Name = "textBoxM";
            this.textBoxM.Size = new System.Drawing.Size(69, 20);
            this.textBoxM.TabIndex = 28;
            this.textBoxM.Text = "1.66";
            this.textBoxM.TextChanged += new System.EventHandler(this.textBoxM_TextChanged);
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(319, 48);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(16, 13);
            this.labelM.TabIndex = 27;
            this.labelM.Text = "M";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(950, 510);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBoxMonitor);
            this.Controls.Add(this.groupBoxCluster);
            this.Controls.Add(this.groupBoxImplication);
            this.Controls.Add(this.groupBoxSettings);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.menuStrip);
            this.MainMenuStrip = this.menuStrip;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MII";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.groupBoxSettings.ResumeLayout(false);
            this.groupBoxSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNumbersFontSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPadding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDotTextFillY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDotTextFillX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIterations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownScaleY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPixel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownScaleX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxX)).EndInit();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.groupBoxImplication.ResumeLayout(false);
            this.groupBoxImplication.PerformLayout();
            this.groupBoxCluster.ResumeLayout(false);
            this.groupBoxMonitor.ResumeLayout(false);
            this.groupBoxMonitor.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Button buttonX;
        private System.Windows.Forms.TextBox textBoxX;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBoxSettings;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem данныеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem трапецииToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem кластерыИТочкиToolStripMenuItem;
        private System.Windows.Forms.Label labelPixel;
        private System.Windows.Forms.NumericUpDown numericUpDownPixel;
        private System.Windows.Forms.Label labelScaleX;
        private System.Windows.Forms.NumericUpDown numericUpDownScaleX;
        private System.Windows.Forms.Label labelMaxX;
        private System.Windows.Forms.NumericUpDown numericUpDownMaxX;
        private System.Windows.Forms.GroupBox groupBoxImplication;
        private System.Windows.Forms.GroupBox groupBoxCluster;
        private System.Windows.Forms.Button buttonCluster;
        private System.Windows.Forms.GroupBox groupBoxMonitor;
        private System.Windows.Forms.Label labelMonitorTimeSet;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelMonitorDots;
        private System.Windows.Forms.Label labelMonitorCluster;
        private System.Windows.Forms.Label labelMonitorTrap;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonTend;
        private System.Windows.Forms.Button buttonSaveSettings;
        private System.Windows.Forms.ToolStripMenuItem временнойРядToolStripMenuItem;
        private System.Windows.Forms.Label labelScaleY;
        private System.Windows.Forms.NumericUpDown numericUpDownScaleY;
        private System.Windows.Forms.TextBox textBoxE;
        private System.Windows.Forms.Label labelE;
        private System.Windows.Forms.Label labelIterations;
        private System.Windows.Forms.NumericUpDown numericUpDownIterations;
        private System.Windows.Forms.Label labelMonitorSettings;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label labelMaxY;
        private System.Windows.Forms.NumericUpDown numericUpDownMaxY;
        private System.Windows.Forms.Label labelDotTextFillX;
        private System.Windows.Forms.NumericUpDown numericUpDownDotTextFillX;
        private System.Windows.Forms.Label labelDotTextFillY;
        private System.Windows.Forms.NumericUpDown numericUpDownDotTextFillY;
        private System.Windows.Forms.Label labelPadding;
        private System.Windows.Forms.NumericUpDown numericUpDownPadding;
        private System.Windows.Forms.Label labelNumbersFontSize;
        private System.Windows.Forms.NumericUpDown numericUpDownNumbersFontSize;
        private System.Windows.Forms.CheckBox checkBoxNeedDrawUnitLine;
        private System.Windows.Forms.TextBox textBoxM;
        private System.Windows.Forms.Label labelM;
    }
}

