﻿using Lab1.Global;
using Lab1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Operations
{
    internal class ClusterOperations
    {
        private Dictionary<int, List<(Cluster Cluster, double U)>> Matrix =
            new Dictionary<int, List<(Cluster Cluster, double U)>>();

        public ClusterOperations(
            List<int> dots,
            List<Cluster> clusters
        )
        {
            var rnd = new Random();
            foreach (var dot in dots.Distinct())
            {
                Matrix.Add(dot, clusters.Select(x => (x, rnd.NextDouble())).ToList());
            }
        }

        private double CalcFunction()
        {
            double sum = 0;
            foreach (var key in Matrix.Keys)
            {
                foreach (var value in Matrix[key])
                {
                    sum += Math.Pow(value.U, Settings.M) * Math.Abs(key - value.Cluster.Center);
                }
            }

            return sum;
        }

        private double CalcCenterSumNumerator(int j)
        {
            double sum = 0;
            foreach (var key in Matrix.Keys)
            {
                sum += Math.Pow(Matrix[key][j].U, 1.6) * key;
            }

            return sum;
        }

        private double CalsCenterDenumenator(int j)
        {
            double sum = 0;
            foreach (var key in Matrix.Keys)
            {
                sum += Math.Pow(Matrix[key][j].U, 1.6);
            }

            return sum;
        }

        private void CalcCenters()
        {
            var key = Matrix.Keys.FirstOrDefault();
            for (int j = 0; j < Matrix[key].Count; j++)
            {
                Matrix[key][j].Cluster.Center = CalcCenterSumNumerator(j) / CalsCenterDenumenator(j);
            }
        }

        private double ReCalcCenterDenumenator(int xi, double clusterCenterJ)
        {
            var key = Matrix.Keys.FirstOrDefault();
            double sum = 0;
            foreach (var clusterCenter in Matrix[key].Select(x => x.Cluster.Center))
            {
                var divider = Math.Abs(xi - clusterCenter);
                var s = Math.Abs(xi - clusterCenterJ);
                sum += s == 0 ? 1 :
                    Math.Pow(s / (divider == 0 ? Settings.E : divider), 3.33);
            }

            return sum;
        }

        private void SetZero(int key, int skipJ)
        {
            for (int j = 0; j < Matrix[key].Count; j++)
            {
                if (j == skipJ) continue;
                Matrix[key][j] = (Matrix[key][j].Cluster, 0);
            }
        }

        private void ReCalcMatrix()
        {
            foreach (var key in Matrix.Keys)
            {
                for (int j = 0; j < Matrix[key].Count; j++)
                {
                    var result = 1 / ReCalcCenterDenumenator(key, Matrix[key][j].Cluster.Center);

                    Matrix[key][j] = (Matrix[key][j].Cluster, result > 1 ? 1 : result);

                    if (Matrix[key][j].U == 1)
                    {
                        SetZero(key, j);
                        break;
                    }
                }
            }
        }

        private void FillClustersData()
        {
            var key = Matrix.Keys.FirstOrDefault();
            for (int j = 0; j < Matrix[key].Count; j++)
            {
                var seq = Matrix.Select(x => new { key = x.Key, value = x.Value[j].U }).Where(x => x.value > 0);
                var minX = seq.Min(x => x.value);
                var maxX = seq.Max(x => x.value);

                Matrix[key][j].Cluster.MinObjIndex = seq.Where(x => x.value == minX).FirstOrDefault().key;
                Matrix[key][j].Cluster.MaxObjIndex = seq.Where(x => x.value == maxX).FirstOrDefault().key;
            }
        }

        public List<Cluster> Execute()
        {
            double oldEps = 0;
            for (int i = 0; i < Settings.Iterations; i++)
            {
                CalcCenters();
                ReCalcMatrix();
                var functionValue = CalcFunction();

                if ((functionValue - oldEps) <= Settings.E)
                    break;
            }

            FillClustersData();
            return Matrix[Matrix.FirstOrDefault().Key].Select(x => x.Cluster).ToList();
        }
    }
}
