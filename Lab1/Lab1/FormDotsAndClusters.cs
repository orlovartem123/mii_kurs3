﻿using Lab1.Global;
using Lab1.Models;
using System;
using System.Linq;
using System.Windows.Forms;

namespace Lab1
{
    public partial class FormDotsAndClusters : Form
    {
        public FormDotsAndClusters()
        {
            InitializeComponent();
            LoadData();
        }

        private void LoadData()
        {
            listBoxCluster.Items.AddRange(DataProvider.Clusters.Select(x => (object)x.Name).ToArray());
            listBoxWeight.Items.AddRange(DataProvider.Dots.Select(x => (object)x).ToArray());
        }

        private void buttonAddCluster_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textBoxCluster.Text) &&
                listBoxCluster.Items.IndexOf(textBoxCluster.Text) == -1)
            {
                listBoxCluster.Items.Add(textBoxCluster.Text);
            }
            else
            {
                var name = $"CLSTR{new Random().Next(Settings.MaxX)}";
                while (listBoxCluster.Items.IndexOf(name) != -1)
                {
                    name = $"CLSTR{new Random().Next(Settings.MaxX)}";
                }
                listBoxCluster.Items.Add(name);
            }
        }

        private void buttonDelCluster_Click(object sender, EventArgs e)
        {
            if (listBoxCluster.Items.Count < 1)
                return;

            var selected = listBoxCluster.SelectedItems.Count > 0 ?
                listBoxCluster.SelectedItems[0] :
                listBoxCluster.Items[listBoxCluster.Items.Count - 1];

            listBoxCluster.Items.Remove(selected);
        }

        private void buttonAddWeight_Click(object sender, EventArgs e)
        {
            if (int.TryParse(textBoxWeight.Text, out int result) &&
                listBoxWeight.Items.IndexOf(result.ToString()) == -1)
            {
                listBoxWeight.Items.Add(result.ToString());
            }
            else
            {
                var name = new Random().Next(Settings.MaxX);
                while (listBoxWeight.Items.IndexOf(name) != -1)
                {
                    name = new Random().Next(Settings.MaxX);
                }
                listBoxWeight.Items.Add(name);
            }
        }

        private void buttonDelWeight_Click(object sender, EventArgs e)
        {
            if (listBoxWeight.Items.Count < 1)
                return;

            var selected = listBoxWeight.SelectedItems.Count > 0 ?
                listBoxWeight.SelectedItems[0] :
                listBoxWeight.Items[listBoxWeight.Items.Count - 1];

            listBoxWeight.Items.Remove(selected);
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            DataProvider.Clusters.Clear();
            DataProvider.Dots.Clear();

            //save clusters
            if (listBoxCluster.Items.Count > 0)
            {
                foreach (var el in listBoxCluster.Items)
                {
                    DataProvider.Clusters.Add(new Cluster(el.ToString()));
                }
            }

            //save dots
            if (listBoxWeight.Items.Count > 0)
            {
                foreach (var el in listBoxWeight.Items)
                {
                    DataProvider.Dots.Add((int)el);
                }
            }

            Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
