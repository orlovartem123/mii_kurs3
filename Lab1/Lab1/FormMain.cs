﻿using Lab1.Extensions;
using Lab1.Global;
using Lab1.Operations;
using System;
using System.Windows.Forms;

namespace Lab1
{
    public partial class FormMain : Form
    {
        private void SetSettings()
        {
            Settings.SetSettings(
                (int)numericUpDownPixel.Value,
                (int)numericUpDownScaleX.Value,
                (int)numericUpDownScaleY.Value,
                (int)numericUpDownMaxX.Value,
                (int)numericUpDownMaxY.Value,
                (int)numericUpDownIterations.Value,
                (int)numericUpDownDotTextFillX.Value,
                (int)numericUpDownDotTextFillY.Value,
                (int)numericUpDownPadding.Value,
                (int)numericUpDownNumbersFontSize.Value,
                checkBoxNeedDrawUnitLine.Checked,
                double.TryParse(textBoxE.Text?.Replace('.', ','), out double e) ? (double?)e : null,
                double.TryParse(textBoxM.Text?.Replace('.', ','), out double m) ? (double?)m : null
            );

            labelMaxX.DownUnsavedSettings(Settings.SettingsDict, nameof(Settings.MaxX));
            labelMaxY.DownUnsavedSettings(Settings.SettingsDict, nameof(Settings.MaxY));
            labelScaleX.DownUnsavedSettings(Settings.SettingsDict, nameof(Settings.ScaleX));
            labelScaleY.DownUnsavedSettings(Settings.SettingsDict, nameof(Settings.ScaleY));
            labelPixel.DownUnsavedSettings(Settings.SettingsDict, nameof(Settings.Pixel));
            labelMaxY.DownUnsavedSettings(Settings.SettingsDict, nameof(Settings.MaxY));
            labelNumbersFontSize.DownUnsavedSettings(Settings.SettingsDict, nameof(Settings.NumbersFontSize));
            labelPadding.DownUnsavedSettings(Settings.SettingsDict, nameof(Settings.Padding));
            labelDotTextFillX.DownUnsavedSettings(Settings.SettingsDict, nameof(Settings.DotTextFillX));
            labelDotTextFillY.DownUnsavedSettings(Settings.SettingsDict, nameof(Settings.DotTextFillY));
            labelIterations.DownUnsavedSettings(Settings.SettingsDict, nameof(Settings.Iterations));
            labelE.DownUnsavedSettings(Settings.SettingsDict, nameof(Settings.E));
            labelM.DownUnsavedSettings(Settings.SettingsDict, nameof(Settings.M));
            checkBoxNeedDrawUnitLine.DownUnsavedSettings(Settings.SettingsDict, nameof(Settings.NeedDrawUnitLine));

            labelMonitorSettings.SetLabelSettingsText();
        }

        private bool DrawAxis()
        {
            var g = pictureBox.GetGraphics();
            var drawResultX = g.DrawXAxis(Font, pictureBox.Width, pictureBox.Height);
            var drawResultY = g.DrawYAxis(Font, pictureBox.Width, pictureBox.Height);

            if (!string.IsNullOrEmpty(drawResultX))
            {
                MessageBox.Show(drawResultX, "Ошибка по X", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            numericUpDownScaleX.Value = Settings.ScaleX;

            if (!string.IsNullOrEmpty(drawResultY))
            {
                MessageBox.Show(drawResultY, "Ошибка по Y", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            numericUpDownScaleY.Value = Settings.ScaleY;

            return true;
        }

        public FormMain()
        {
            InitializeComponent();
            SetSettings();
            labelMonitorSettings.SetLabelSettingsText();
            labelMonitorTrap.SetLabelTrapsText();
            labelMonitorDots.SetLabelDotsText();
            labelMonitorCluster.SetLabelClusterText();
            labelMonitorTimeSet.SetLabelTimeSetText();
        }

        #region SettingsChekers

        private void checkBoxNeedDrawUnit_CheckedChanged(object sender, EventArgs e)
        {
            var old = Settings.NeedDrawUnitLine;
            if (old != checkBoxNeedDrawUnitLine.Checked)
            {
                checkBoxNeedDrawUnitLine.UpUnsavedSettings(Settings.SettingsDict, nameof(Settings.NeedDrawUnitLine));
            }
            else
            {
                checkBoxNeedDrawUnitLine.DownUnsavedSettings(Settings.SettingsDict, nameof(Settings.NeedDrawUnitLine));
            }

            labelMonitorSettings.SetLabelSettingsText();
        }

        private void numericUpDownMaxX_ValueChanged(object sender, EventArgs e)
        {
            var old = Settings.MaxX;
            if (old != numericUpDownMaxX.Value)
            {
                labelMaxX.UpUnsavedSettings(Settings.SettingsDict, nameof(Settings.MaxX));
            }
            else
            {
                labelMaxX.DownUnsavedSettings(Settings.SettingsDict, nameof(Settings.MaxX));
            }

            labelMonitorSettings.SetLabelSettingsText();
        }

        private void numericUpDownScaleX_ValueChanged(object sender, EventArgs e)
        {
            var old = Settings.ScaleX;
            if (old != numericUpDownScaleX.Value)
            {
                labelScaleX.UpUnsavedSettings(Settings.SettingsDict, nameof(Settings.ScaleX));
            }
            else
            {
                labelScaleX.DownUnsavedSettings(Settings.SettingsDict, nameof(Settings.ScaleX));
            }

            labelMonitorSettings.SetLabelSettingsText();
        }

        private void numericUpDownScaleY_ValueChanged(object sender, EventArgs e)
        {
            var old = Settings.ScaleY;
            if (old != numericUpDownScaleY.Value)
            {
                labelScaleY.UpUnsavedSettings(Settings.SettingsDict, nameof(Settings.ScaleY));
            }
            else
            {
                labelScaleY.DownUnsavedSettings(Settings.SettingsDict, nameof(Settings.ScaleY));
            }

            labelMonitorSettings.SetLabelSettingsText();
        }

        private void numericUpDownPixel_ValueChanged(object sender, EventArgs e)
        {
            var old = Settings.Pixel;
            if (old != numericUpDownPixel.Value)
            {
                labelPixel.UpUnsavedSettings(Settings.SettingsDict, nameof(Settings.Pixel));
            }
            else
            {
                labelPixel.DownUnsavedSettings(Settings.SettingsDict, nameof(Settings.Pixel));
            }

            labelMonitorSettings.SetLabelSettingsText();
        }

        private void numericUpDownIterations_ValueChanged(object sender, EventArgs e)
        {
            var old = Settings.Iterations;
            if (old != numericUpDownIterations.Value)
            {
                labelIterations.UpUnsavedSettings(Settings.SettingsDict, nameof(Settings.Iterations));
            }
            else
            {
                labelIterations.DownUnsavedSettings(Settings.SettingsDict, nameof(Settings.Iterations));
            }

            labelMonitorSettings.SetLabelSettingsText();
        }

        private void textBoxE_TextChanged(object sender, EventArgs e)
        {
            var old = Settings.E;
            if (!double.TryParse(textBoxE.Text?.Replace('.', ','), out double result))
            {
                return;
            }

            if (old != result)
            {
                labelE.UpUnsavedSettings(Settings.SettingsDict, nameof(Settings.E));
            }
            else
            {
                labelE.DownUnsavedSettings(Settings.SettingsDict, nameof(Settings.E));
            }

            labelMonitorSettings.SetLabelSettingsText();
        }

        private void textBoxM_TextChanged(object sender, EventArgs e)
        {
            var old = Settings.M;
            if (!double.TryParse(textBoxM.Text?.Replace('.', ','), out double result))
            {
                return;
            }

            if (old != result)
            {
                labelM.UpUnsavedSettings(Settings.SettingsDict, nameof(Settings.M));
            }
            else
            {
                labelM.DownUnsavedSettings(Settings.SettingsDict, nameof(Settings.M));
            }

            labelMonitorSettings.SetLabelSettingsText();
        }

        private void numericUpDownMaxY_ValueChanged(object sender, EventArgs e)
        {
            var old = Settings.MaxY;
            if (old != numericUpDownMaxY.Value)
            {
                labelMaxY.UpUnsavedSettings(Settings.SettingsDict, nameof(Settings.MaxY));
            }
            else
            {
                labelMaxY.DownUnsavedSettings(Settings.SettingsDict, nameof(Settings.MaxY));
            }

            labelMonitorSettings.SetLabelSettingsText();
        }

        private void numericUpDownDotTextFill_ValueChanged(object sender, EventArgs e)
        {
            var old = Settings.DotTextFillX;
            if (old != numericUpDownDotTextFillX.Value)
            {
                labelDotTextFillX.UpUnsavedSettings(Settings.SettingsDict, nameof(Settings.DotTextFillX));
            }
            else
            {
                labelDotTextFillX.DownUnsavedSettings(Settings.SettingsDict, nameof(Settings.DotTextFillX));
            }

            labelMonitorSettings.SetLabelSettingsText();
        }

        private void numericUpDownDotTextFillY_ValueChanged(object sender, EventArgs e)
        {
            var old = Settings.DotTextFillY;
            if (old != numericUpDownDotTextFillY.Value)
            {
                labelDotTextFillY.UpUnsavedSettings(Settings.SettingsDict, nameof(Settings.DotTextFillY));
            }
            else
            {
                labelDotTextFillX.DownUnsavedSettings(Settings.SettingsDict, nameof(Settings.DotTextFillY));
            }

            labelMonitorSettings.SetLabelSettingsText();
        }

        private void numericUpDownPadding_ValueChanged(object sender, EventArgs e)
        {
            var old = Settings.Padding;
            if (old != numericUpDownPadding.Value)
            {
                labelPadding.UpUnsavedSettings(Settings.SettingsDict, nameof(Settings.Padding));
            }
            else
            {
                labelPadding.DownUnsavedSettings(Settings.SettingsDict, nameof(Settings.Padding));
            }

            labelMonitorSettings.SetLabelSettingsText();
        }

        private void numericUpDownNumbersFontSize_ValueChanged(object sender, EventArgs e)
        {
            var old = Settings.NumbersFontSize;
            if (old != numericUpDownNumbersFontSize.Value)
            {
                labelNumbersFontSize.UpUnsavedSettings(Settings.SettingsDict, nameof(Settings.NumbersFontSize));
            }
            else
            {
                labelNumbersFontSize.DownUnsavedSettings(Settings.SettingsDict, nameof(Settings.NumbersFontSize));
            }

            labelMonitorSettings.SetLabelSettingsText();
        }

        #endregion

        private void buttonSaveSettings_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBoxE.Text?.Replace('.', ','), out double result))
            {
                textBoxE.Text = Settings.E.ToString();
            }

            SetSettings();
        }

        private void buttonCluster_Click(object sender, EventArgs e)
        {
            if (!DrawAxis())
            {
                return;
            }

            if (DataProvider.Clusters.Count == 0)
            {
                MessageBox.Show("Кластеры не найдены", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (DataProvider.Dots.Count == 0)
            {
                MessageBox.Show("Точки не найдены", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var result = new ClusterOperations(DataProvider.Dots, DataProvider.Clusters).Execute();
            var g = pictureBox.GetGraphics(false);
            g.DrawClusters(Font, result, pictureBox.Width, pictureBox.Height);
        }

        private void кластерыИТочкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new FormDotsAndClusters();
            form.ShowDialog();

            labelMonitorCluster.SetLabelClusterText();
            labelMonitorDots.SetLabelDotsText();
        }
    }
}
