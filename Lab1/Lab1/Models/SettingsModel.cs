﻿namespace Lab1.Models
{
    internal class SettingsModel
    {
        public object SettingValue { get; set; }

        public bool Actual { get; set; }
    }
}
