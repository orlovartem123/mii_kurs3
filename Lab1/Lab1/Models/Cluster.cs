﻿using System;
using System.Drawing;

namespace Lab1.Models
{
    internal class Cluster
    {
        public Cluster(string name)
        {
            Name = name;
            var rnd = new Random();
            Color = Color.FromArgb(rnd.Next(255), rnd.Next(255), rnd.Next(255));
        }

        public string Name { get; set; }

        public double Center { get; set; } = -1;

        public Color Color { get; set; }

        public int MinObjIndex { get; set; } = -1;

        public int MaxObjIndex { get; set; } = -1;
    }
}
