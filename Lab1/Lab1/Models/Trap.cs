﻿
namespace Lab1.Models
{
    internal class Trap
    {
        public string Name { get; set; }

        public int X1 { get; set; }

        public int X2 { get; set; }

        public int X3 { get; set; }

        public int X4 { get; set; }
    }
}
