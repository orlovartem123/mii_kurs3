﻿namespace Lab1
{
    partial class FormDotsAndClusters
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxCluster = new System.Windows.Forms.ListBox();
            this.listBoxWeight = new System.Windows.Forms.ListBox();
            this.buttonDelCluster = new System.Windows.Forms.Button();
            this.buttonAddCluster = new System.Windows.Forms.Button();
            this.textBoxCluster = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxWeight = new System.Windows.Forms.TextBox();
            this.buttonDelWeight = new System.Windows.Forms.Button();
            this.buttonAddWeight = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listBoxCluster
            // 
            this.listBoxCluster.FormattingEnabled = true;
            this.listBoxCluster.Location = new System.Drawing.Point(191, 34);
            this.listBoxCluster.Name = "listBoxCluster";
            this.listBoxCluster.Size = new System.Drawing.Size(197, 121);
            this.listBoxCluster.TabIndex = 23;
            // 
            // listBoxWeight
            // 
            this.listBoxWeight.FormattingEnabled = true;
            this.listBoxWeight.Location = new System.Drawing.Point(15, 34);
            this.listBoxWeight.Name = "listBoxWeight";
            this.listBoxWeight.Size = new System.Drawing.Size(144, 121);
            this.listBoxWeight.TabIndex = 22;
            // 
            // buttonDelCluster
            // 
            this.buttonDelCluster.Location = new System.Drawing.Point(354, 160);
            this.buttonDelCluster.Name = "buttonDelCluster";
            this.buttonDelCluster.Size = new System.Drawing.Size(34, 23);
            this.buttonDelCluster.TabIndex = 21;
            this.buttonDelCluster.Text = "-";
            this.buttonDelCluster.UseVisualStyleBackColor = true;
            this.buttonDelCluster.Click += new System.EventHandler(this.buttonDelCluster_Click);
            // 
            // buttonAddCluster
            // 
            this.buttonAddCluster.Location = new System.Drawing.Point(314, 160);
            this.buttonAddCluster.Name = "buttonAddCluster";
            this.buttonAddCluster.Size = new System.Drawing.Size(34, 23);
            this.buttonAddCluster.TabIndex = 20;
            this.buttonAddCluster.Text = "+";
            this.buttonAddCluster.UseVisualStyleBackColor = true;
            this.buttonAddCluster.Click += new System.EventHandler(this.buttonAddCluster_Click);
            // 
            // textBoxCluster
            // 
            this.textBoxCluster.Location = new System.Drawing.Point(191, 162);
            this.textBoxCluster.Name = "textBoxCluster";
            this.textBoxCluster.Size = new System.Drawing.Size(117, 20);
            this.textBoxCluster.TabIndex = 19;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(188, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Кластеры";
            // 
            // textBoxWeight
            // 
            this.textBoxWeight.Location = new System.Drawing.Point(15, 161);
            this.textBoxWeight.Name = "textBoxWeight";
            this.textBoxWeight.Size = new System.Drawing.Size(64, 20);
            this.textBoxWeight.TabIndex = 17;
            // 
            // buttonDelWeight
            // 
            this.buttonDelWeight.Location = new System.Drawing.Point(125, 159);
            this.buttonDelWeight.Name = "buttonDelWeight";
            this.buttonDelWeight.Size = new System.Drawing.Size(34, 23);
            this.buttonDelWeight.TabIndex = 16;
            this.buttonDelWeight.Text = "-";
            this.buttonDelWeight.UseVisualStyleBackColor = true;
            this.buttonDelWeight.Click += new System.EventHandler(this.buttonDelWeight_Click);
            // 
            // buttonAddWeight
            // 
            this.buttonAddWeight.Location = new System.Drawing.Point(85, 159);
            this.buttonAddWeight.Name = "buttonAddWeight";
            this.buttonAddWeight.Size = new System.Drawing.Size(34, 23);
            this.buttonAddWeight.TabIndex = 15;
            this.buttonAddWeight.Text = "+";
            this.buttonAddWeight.UseVisualStyleBackColor = true;
            this.buttonAddWeight.Click += new System.EventHandler(this.buttonAddWeight_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Веса";
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(308, 241);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(80, 23);
            this.buttonOk.TabIndex = 24;
            this.buttonOk.Text = "Ок";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(222, 241);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(80, 23);
            this.buttonCancel.TabIndex = 25;
            this.buttonCancel.Text = "Отмена";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // FormDotsAndClusters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(406, 276);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.listBoxCluster);
            this.Controls.Add(this.listBoxWeight);
            this.Controls.Add(this.buttonDelCluster);
            this.Controls.Add(this.buttonAddCluster);
            this.Controls.Add(this.textBoxCluster);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxWeight);
            this.Controls.Add(this.buttonDelWeight);
            this.Controls.Add(this.buttonAddWeight);
            this.Controls.Add(this.label1);
            this.Name = "FormDotsAndClusters";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Точки и кластеры";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxCluster;
        private System.Windows.Forms.ListBox listBoxWeight;
        private System.Windows.Forms.Button buttonDelCluster;
        private System.Windows.Forms.Button buttonAddCluster;
        private System.Windows.Forms.TextBox textBoxCluster;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxWeight;
        private System.Windows.Forms.Button buttonDelWeight;
        private System.Windows.Forms.Button buttonAddWeight;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
    }
}