﻿using Lab1.Models;
using System.Collections.Generic;

namespace Lab1.Global
{
    internal class DataProvider
    {
        public static List<Trap> Traps { get; set; } = new List<Trap>();

        public static List<int> Dots { get; set; } = new List<int>();

        public static List<Cluster> Clusters { get; set; } = new List<Cluster>();

        public static List<int> TimeSet { get; set; } = new List<int>();
    }
}
