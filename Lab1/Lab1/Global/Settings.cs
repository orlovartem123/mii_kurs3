﻿using Lab1.Extensions;
using Lab1.Models;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Lab1.Global
{
    internal class Settings
    {
        #region Settings

        /// <summary>
        /// Высота деления
        /// </summary>
        public static int UnitDrawHeight { get; set; } = 5;

        /// <summary>
        /// name - value
        /// </summary>
        public static Dictionary<string, SettingsModel> SettingsDict { get; } = new Dictionary<string, SettingsModel>();

        public static int Pixel
        {
            get => SettingsDict.GetSettingOrDefault<int>(nameof(Pixel));
            set => SettingsDict.TrySetSetting(nameof(Pixel), value);
        }

        public static int ScaleX
        {
            get => SettingsDict.GetSettingOrDefault<int>(nameof(ScaleX));
            set => SettingsDict.TrySetSetting(nameof(ScaleX), value);
        }

        public static int ScaleY
        {
            get => SettingsDict.GetSettingOrDefault<int>(nameof(ScaleY));
            set => SettingsDict.TrySetSetting(nameof(ScaleY), value);
        }

        public static int MaxX
        {
            get => SettingsDict.GetSettingOrDefault<int>(nameof(MaxX));
            set => SettingsDict.TrySetSetting(nameof(MaxX), value);
        }

        public static int MaxY
        {
            get => SettingsDict.GetSettingOrDefault<int>(nameof(MaxY));
            set => SettingsDict.TrySetSetting(nameof(MaxY), value);
        }

        public static int DotTextFillX
        {
            get => SettingsDict.GetSettingOrDefault<int>(nameof(DotTextFillX));
            set => SettingsDict.TrySetSetting(nameof(DotTextFillX), value);
        }

        public static int DotTextFillY
        {
            get => SettingsDict.GetSettingOrDefault<int>(nameof(DotTextFillY));
            set => SettingsDict.TrySetSetting(nameof(DotTextFillY), value);
        }

        public static int Iterations
        {
            get => SettingsDict.GetSettingOrDefault<int>(nameof(Iterations));
            set => SettingsDict.TrySetSetting(nameof(Iterations), value);
        }

        public static int Padding
        {
            get => SettingsDict.GetSettingOrDefault<int>(nameof(Padding));
            set => SettingsDict.TrySetSetting(nameof(Padding), value);
        }

        public static int NumbersFontSize
        {
            get => SettingsDict.GetSettingOrDefault<int>(nameof(NumbersFontSize));
            set => SettingsDict.TrySetSetting(nameof(NumbersFontSize), value);
        }

        public static double E
        {
            get => SettingsDict.GetSettingOrDefault<double>(nameof(E));
            set => SettingsDict.TrySetSetting(nameof(E), value);
        }

        public static double M
        {
            get => SettingsDict.GetSettingOrDefault<double>(nameof(M));
            set => SettingsDict.TrySetSetting(nameof(M), value);
        }

        public static int UnsavedSettings
        {
            get => SettingsDict.Where(x => !x.Value.Actual).Count();
        }

        public static bool NeedDrawUnitLine
        {
            get => SettingsDict.GetSettingOrDefault<bool>(nameof(NeedDrawUnitLine));
            set => SettingsDict.TrySetSetting(nameof(NeedDrawUnitLine), value);
        }

        public static void SetSettings(
            int? pixel = null,
            int? scaleX = null,
            int? scaleY = null,
            int? maxX = null,
            int? maxY = null,
            int? iterations = null,
            int? dotTextFillX = null,
            int? dotTextFillY = null,
            int? padding = null,
            int? numbersFontSize = null,
            bool? needDrawUnitLine = null,
            double? e = null,
            double? m = null
        )
        {
            if (pixel.HasValue)
                Pixel = pixel.Value;
            if (scaleX.HasValue)
                ScaleX = scaleX.Value;
            if (scaleY.HasValue)
                ScaleY = scaleY.Value;
            if (maxX.HasValue)
                MaxX = maxX.Value;
            if (maxY.HasValue)
                MaxY = maxY.Value;
            if (iterations.HasValue)
                Iterations = iterations.Value;
            if (dotTextFillX.HasValue)
                DotTextFillX = dotTextFillX.Value;
            if (dotTextFillY.HasValue)
                DotTextFillY = dotTextFillY.Value;
            if (padding.HasValue)
                Padding = padding.Value;
            if (numbersFontSize.HasValue)
                NumbersFontSize = numbersFontSize.Value;
            if (needDrawUnitLine.HasValue)
                NeedDrawUnitLine = needDrawUnitLine.Value;
            if (e.HasValue)
                E = e.Value;
            if (m.HasValue)
                M = m.Value;

            SettingsDict.SaveAllSettings();
        }

        #endregion

        #region Monitor

        public static Color GoodColor { get => Color.Green; }

        public static Color BadColor { get => Color.Red; }

        public static Color WarningColor { get => Color.DarkOrange; }

        public static Color DefaultColor { get => Color.Black; }

        public static (string Text, Color TextColor) TrapMonitorText
        {
            get
            {
                return DataProvider.Traps.Count > 0 ?
                    (DataProvider.Traps.GetTextTrap(), GoodColor) :
                    ("Трапеций не обнаружено", BadColor);
            }
        }

        public static (string Text, Color TextColor) DotsMonitorData
        {
            get
            {
                return DataProvider.Dots.Count > 0 ?
                    (DataProvider.Dots.GetTextDots(), GoodColor) :
                    ("Точек не обнаружено", BadColor);
            }
        }

        public static (string Text, Color TextColor) ClusterMonitorData
        {
            get
            {
                return DataProvider.Clusters.Count > 0 ?
                    (DataProvider.Clusters.GetTextClusters(), GoodColor) :
                    ("Кластеров не обнаружено", BadColor);
            }
        }

        public static (string Text, Color TextColor) TimeSetMonitorData
        {
            get
            {
                return DataProvider.TimeSet.Count > 0 ?
                    ($"Временной ряд обнаружен", GoodColor) :
                    ("Временной ряд не обнаружен", BadColor);
            }
        }

        public static (string Text, Color TextColor) SettingsMonitorData
        {
            get
            {
                return UnsavedSettings == 0 ?
                    ($"Настройки актуальны", GoodColor) :
                    (UnsavedSettings.GetTextUnsavedSettings(), WarningColor);
            }
        }

        #endregion
    }
}
