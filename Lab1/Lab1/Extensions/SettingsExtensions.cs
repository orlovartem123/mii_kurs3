﻿using Lab1.Models;
using System.Collections.Generic;

namespace Lab1.Extensions
{
    internal static class SettingsExtensions
    {
        public static T GetSettingOrDefault<T>(
            this Dictionary<string, SettingsModel> settings,
            string settingName
        )
        {
            if (settings.TryGetValue(settingName, out SettingsModel setting) &&
                    setting != default)
                return (T)setting.SettingValue;

            return default(T);
        }

        public static void TrySetSetting<T>(
            this Dictionary<string, SettingsModel> settings,
            string settingName,
            T settingValue
        )
        {
            if (settings.ContainsKey(settingName))
            {
                settings[settingName].SettingValue = settingValue;
                settings[settingName].Actual = true;
            }
            else
            {
                settings.Add(settingName, new SettingsModel { SettingValue = settingValue, Actual = true });
            }
        }

        public static void TrySetSettingActual(
            this Dictionary<string, SettingsModel> settings,
            string settingName,
            bool actual
        )
        {
            if (settings.ContainsKey(settingName))
            {
                settings[settingName].Actual = actual;
            }
        }

        public static void SaveAllSettings(
            this Dictionary<string, SettingsModel> settings
        )
        {
            if (settings != null && settings.Count > 0)
            {
                foreach (var el in settings)
                {
                    el.Value.Actual = true;
                }
            }
        }
    }
}
