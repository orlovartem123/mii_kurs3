﻿using Lab1.Models;
using System.Collections.Generic;

namespace Lab1.Extensions
{
    internal static class MonitorDataExtensions
    {
        public static string GetTextTrap(this List<Trap> value)
        {
            var trapCount = value.Count;
            var mod = trapCount % 100;
            if (mod > 10 && mod < 20)
                return $"Обнаружено {trapCount} трапеций";
            else
            {
                var mod2 = mod % 10;
                if (mod2 == 1)
                    return $"Обнаружена 1 трапеция";

                if (mod2 > 1 && mod2 < 5)
                    return $"Обнаружены {trapCount} трапеции";

                return $"Обнаружено {trapCount} трапеций";
            }
        }

        public static string GetTextDots(this List<int> value)
        {
            var dotsCount = value.Count;
            var mod = dotsCount % 100;
            if (mod > 10 && mod < 20)
                return $"Обнаружено {dotsCount} точек";
            else
            {
                var mod2 = mod % 10;
                if (mod2 == 1)
                    return $"Обнаружена 1 точка";

                if (mod2 > 1 && mod2 < 5)
                    return $"Обнаружены {dotsCount} точки";

                return $"Обнаружено {dotsCount} точек";
            }
        }

        public static string GetTextClusters(this List<Cluster> value)
        {
            var clustersCount = value.Count;
            var mod = clustersCount % 100;
            if (mod > 10 && mod < 20)
                return $"Обнаружено {clustersCount} кластеров";
            else
            {
                var mod2 = mod % 10;
                if (mod2 == 1)
                    return $"Обнаружен 1 кластер";

                if (mod2 > 1 && mod2 < 5)
                    return $"Обнаружены {clustersCount} кластера";

                return $"Обнаружено {clustersCount} кластеров";
            }
        }

        public static string GetTextUnsavedSettings(this int settingCount)
        {
            var mod = settingCount % 100;
            if (mod > 10 && mod < 20)
                return $"{settingCount} несохранненых настроек";
            else
            {
                var mod2 = mod % 10;
                if (mod2 == 1)
                    return $"1 несохраненная настройка";

                if (mod2 > 1 && mod2 < 5)
                    return $"{settingCount} несохраненные настройки";

                return $"{settingCount} несохраненных настроек";
            }
        }
    }
}
