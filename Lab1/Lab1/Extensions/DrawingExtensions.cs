﻿using Lab1.Global;
using Lab1.Models;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Lab1.Extensions
{
    internal static class DrawingExtensions
    {
        public static Graphics GetGraphics(this PictureBox pb, bool needClear = true)
        {
            if (pb == null)
            {
                return null;
            }

            if (needClear)
            {
                pb.Image = null;
                pb.Refresh();
            }

            return pb.CreateGraphics();
        }

        public static void DrawClusters(this Graphics g, Font font, List<Cluster> clusters, int gXLen, int gYLen)
        {
            foreach (var cluster in clusters)
            {
                var pen = new Pen(cluster.Color);
                var rect = new Rectangle(
                    cluster.MinObjIndex * Settings.ScaleX + Settings.Padding,
                    gYLen - 2 * Settings.Padding,
                    (cluster.MaxObjIndex - cluster.MinObjIndex) * Settings.ScaleX,
                    2 * Settings.Padding);
                g.DrawEllipse(pen, rect);
            }
        }

        public static void DrawTextX(Graphics g, Font font, string text, int x, int y)
        {
            var f = new Font(font.FontFamily, Settings.NumbersFontSize);
            var size = g.MeasureString(text, f);
            var pt = new Point(x - (int)size.Width / 2, y + 1);
            var rect = new RectangleF(pt, size);
            g.DrawString(text, f, Brushes.Black, rect);
        }

        public static void DrawTextY(Graphics g, Font font, string text, int x, int y)
        {
            var f = new Font(font.FontFamily, Settings.NumbersFontSize);
            var size = g.MeasureString(text, f);
            var xDraw = x - (int)size.Width - 3;
            if (xDraw < 0)
                xDraw = 0;

            var pt = new Point(xDraw, y - (int)size.Height / 2);
            var rect = new RectangleF(pt, size);
            g.DrawString(text, f, Brushes.Black, rect);
        }

        public static string DrawXAxis(this Graphics g, Font font, int gXLen, int gYLen)
        {
            //Подбираем масштаб
            var maxX = Settings.MaxX;
            var scaleX = Settings.ScaleX;
            var padding = Settings.Padding;
            while ((gXLen - 2 * padding) < (scaleX * maxX))
            {
                scaleX--;
                if (scaleX < 1)
                    return $"Слишком большой масштаб. Текущее значение {Settings.ScaleX}";
            }
            Settings.ScaleX = scaleX;

            var pen = new Pen(Settings.DefaultColor);

            //X-Axis
            g.DrawLine(
                pen,
                new Point(padding, gYLen - padding),
                new Point(gXLen - padding, gYLen - padding)
            );

            //Стрелка
            g.DrawLine(
                pen,
                new Point(gXLen - padding, gYLen - padding),
                new Point(gXLen - padding - Settings.UnitDrawHeight, gYLen - padding - Settings.UnitDrawHeight)
            );
            g.DrawLine(
                pen,
                new Point(gXLen - padding, gYLen - padding),
                new Point(gXLen - padding - Settings.UnitDrawHeight, gYLen - padding + Settings.UnitDrawHeight)
            );

            var needDrawTextMod = (int)(((double)1 / (double)Settings.DotTextFillX) * 100);
            if (Settings.NeedDrawUnitLine)
            {
                for (int i = 0; i < maxX; i++)
                {
                    g.DrawLine(
                        pen,
                        new Point(i * scaleX + padding, gYLen - padding - Settings.UnitDrawHeight),
                        new Point(i * scaleX + padding, gYLen - padding + Settings.UnitDrawHeight)
                    );

                    if (i % needDrawTextMod == 0)
                    {
                        DrawTextX(g, font, i.ToString(), i * scaleX + padding, gYLen - padding);
                    }
                }
            }
            else
            {
                for (int i = 0; i < maxX; i++)
                {
                    if (i % needDrawTextMod == 0)
                    {
                        DrawTextX(g, font, i.ToString(), i * scaleX + padding, gYLen - padding);
                        g.DrawLine(
                            pen,
                            new Point(i * scaleX + padding, gYLen - padding - Settings.UnitDrawHeight),
                            new Point(i * scaleX + padding, gYLen - padding + Settings.UnitDrawHeight)
                        );
                    }
                }
            }

            return null;
        }

        public static string DrawYAxis(this Graphics g, Font font, int gXLen, int gYLen)
        {
            //Подбираем масштаб
            var maxY = Settings.MaxY;
            var scaleY = Settings.ScaleY;
            var padding = Settings.Padding;
            while ((gYLen - 2 * padding) < (scaleY * maxY))
            {
                scaleY--;
                if (scaleY < 1)
                    return $"Слишком большой масштаб. Текущее значение {Settings.ScaleY}";
            }
            Settings.ScaleY = scaleY;

            var pen = new Pen(Settings.DefaultColor);

            //Y-Axis
            g.DrawLine(
                pen,
                new Point(padding, gYLen - padding),
                new Point(padding, padding)
            );

            //Стрелка
            g.DrawLine(
                pen,
                new Point(padding, padding),
                new Point(padding - Settings.UnitDrawHeight, padding + Settings.UnitDrawHeight)
            );
            g.DrawLine(
                pen,
                new Point(padding, padding),
                new Point(padding + Settings.UnitDrawHeight, padding + Settings.UnitDrawHeight)
            );

            var needDrawTextMod = (int)(((double)1 / (double)Settings.DotTextFillX) * 100);
            if (Settings.NeedDrawUnitLine)
            {
                for (int i = 0; i < maxY; i++)
                {
                    g.DrawLine(
                        pen,
                        new Point(padding - Settings.UnitDrawHeight, gYLen - padding - i * scaleY),
                        new Point(padding + Settings.UnitDrawHeight, gYLen - padding - i * scaleY)
                    );

                    if (i % needDrawTextMod == 0)
                    {
                        DrawTextY(g, font, i.ToString(), padding, gYLen - padding - i * scaleY);
                    }
                }
            }
            else
            {
                for (int i = 0; i < maxY; i++)
                {
                    if (i % needDrawTextMod == 0)
                    {
                        DrawTextY(g, font, i.ToString(), padding, gYLen - padding - i * scaleY);
                        g.DrawLine(
                            pen,
                            new Point(padding - Settings.UnitDrawHeight, gYLen - padding - i * scaleY),
                            new Point(padding + Settings.UnitDrawHeight, gYLen - padding - i * scaleY)
                        );
                    }
                }
            }

            return null;
        }

        public static void ReScaleTraps(this Graphics g)
        {

        }

        public static void ReScaleClusters(this Graphics g)
        {

        }
    }
}
