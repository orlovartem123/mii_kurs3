﻿using Lab1.Global;
using Lab1.Models;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Lab1.Extensions
{
    internal static class WinFormsControlsExtensions
    {
        public static void UpUnsavedSettings(
            this Label label,
            Dictionary<string, SettingsModel> settings,
            string settingName
        )
        {
            label.ForeColor = Settings.WarningColor;
            settings.TrySetSettingActual(settingName, false);
        }

        public static void UpUnsavedSettings(
            this CheckBox checkBox,
            Dictionary<string, SettingsModel> settings,
            string settingName
        )
        {
            checkBox.ForeColor = Settings.WarningColor;
            settings.TrySetSettingActual(settingName, false);
        }

        public static void DownUnsavedSettings(
            this Label label,
            Dictionary<string, SettingsModel> settings,
            string settingName
        )
        {
            label.ForeColor = Settings.DefaultColor;
            settings.TrySetSettingActual(settingName, true);
        }

        public static void DownUnsavedSettings(
            this CheckBox checkBox,
            Dictionary<string, SettingsModel> settings,
            string settingName
        )
        {
            checkBox.ForeColor = Settings.DefaultColor;
            settings.TrySetSettingActual(settingName, true);
        }

        public static void SetLabelSettingsText(this Label label)
        {
            var monitorData = Settings.SettingsMonitorData;
            label.Text = monitorData.Text;
            label.ForeColor = monitorData.TextColor;
        }

        public static void SetLabelTrapsText(this Label label)
        {
            var monitorData = Settings.TrapMonitorText;
            label.Text = monitorData.Text;
            label.ForeColor = monitorData.TextColor;
        }

        public static void SetLabelDotsText(this Label label)
        {
            var monitorData = Settings.DotsMonitorData;
            label.Text = monitorData.Text;
            label.ForeColor = monitorData.TextColor;
        }

        public static void SetLabelClusterText(this Label label)
        {
            var monitorData = Settings.ClusterMonitorData;
            label.Text = monitorData.Text;
            label.ForeColor = monitorData.TextColor;
        }

        public static void SetLabelTimeSetText(this Label label)
        {
            var monitorData = Settings.TimeSetMonitorData;
            label.Text = monitorData.Text;
            label.ForeColor = monitorData.TextColor;
        }
    }
}
